/* org-protocol support commands
 *
 * These commands assume you've configured Firefox to fire
 * emacsclient, as described at
 * http://orgmode.org/worg/org-contrib/org-protocol.php
 *
 */
CmdUtils.CreateCommand({
    names: ["remember"],
    homepage: "http://yergler.net/projects/ubiquity-commands/org-mode/",
    author: { name: "Nathan R. Yergler", email: "nathan@yergler.net"},
    license: "GPL 2",
    description: "Remember the current page in Emacs using org-protocol.",
    help: "Sends the current page to Emacs using org-protocol.  If a note is provided, it is passed as the body of the remember item, otherwise the selection is used.",
    preview: 'Remember the current page and selection or note with <a href="http://orgmode.org/worg/org-contrib/org-protocol.php">org-protocol</a>.',
    arguments: [{role: "object",
		 nountype: noun_arb_text,
		 label: "note"}],
    execute: function(arguments) {

        var document = CmdUtils.getDocumentInsecure();
        var note = arguments.object? arguments.object.text : CmdUtils.getSelection();

        document.location.href = 'org-protocol://remember://'+encodeURIComponent(document.location.href)+'/'+encodeURIComponent(document.title)+'/'+encodeURIComponent(note);
    }
});

CmdUtils.CreateCommand({
    names: ["store-link"],
    homepage: "http://yergler.net/projects/ubiquity-commands/org-mode/",
    author: { name: "Nathan R. Yergler", email: "nathan@yergler.net"},
    license: "GPL 2",
    description: "Store the current page URL and title as an insertable org-link.",
    preview: "Store the current page URL and title as an insertable org-link.",
    execute: function() {

        var document = CmdUtils.getDocumentInsecure();

        document.location.href = 'org-protocol://store-link://'+encodeURIComponent(document.location.href)+'/'+encodeURIComponent(document.title);
    }
});
